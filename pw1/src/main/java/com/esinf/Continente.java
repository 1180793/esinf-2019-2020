package com.esinf;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public enum Continente {

        AMERICASUL("AmericaSul"), EUROPA("Europa");

        private String strDesignation;

        private Continente(String strDesignation) {
                this.strDesignation = strDesignation;
        }

        public String getNome() {
                return this.strDesignation;
        }

        @Override
        public String toString() {
                return String.format("%s", this.strDesignation);
        }
        
        public static Continente getContinente(String strContinente) {
                for (Continente oContinente : Continente.values()) {
                        if (oContinente.getNome().equalsIgnoreCase(strContinente)) {
                                return oContinente;
                        }
                }
                return null;
        }
}
