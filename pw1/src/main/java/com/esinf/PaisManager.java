package com.esinf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class PaisManager {

        private List<Pais> lstPaises;

        public PaisManager() {
                this.lstPaises = new ArrayList<>();
        }

        public List<Pais> getPaises() {
                return this.lstPaises;
        }

        public Pais matchPais(String strNome) {
                for (Pais oPais : lstPaises) {
                        if (oPais.getNome().equalsIgnoreCase(strNome)) {
                                return oPais;
                        }
                }
                return null;
        }

        // <editor-fold defaultstate="collapsed" desc="ESINF Exercício 1">
        public void loadPaises(String filePath) {
                List<ArrayList<String>> list = getDataFromFile(filePath, ",");
                for (ArrayList<String> d : list) {
                        String strNome = d.get(0).substring(0, 1).toUpperCase() + d.get(0).substring(1);
                        Continente oContinente = Continente.getContinente(d.get(1));
                        double dblPopulacao = Double.parseDouble(d.get(2));
                        String strCapital = d.get(3).substring(0, 1).toUpperCase() + d.get(3).substring(1);
                        double dblLatitude = Double.parseDouble(d.get(4));
                        double dblLongitude = Double.parseDouble(d.get(5));
                        Pais oPais = new Pais(strNome, oContinente, dblPopulacao, strCapital, dblLatitude, dblLongitude);
                        lstPaises.add(oPais);
                }
        }

        public void loadFronteiras(String filePath) {
                List<ArrayList<String>> list = getDataFromFile(filePath, ",");
                for (ArrayList<String> d : list) {
                        String strPais1 = d.get(0);
                        String strPais2 = d.get(1);
                        Pais oPais1 = matchPais(strPais1);
                        Pais oPais2 = matchPais(strPais2);
                        if ((oPais1 != null && oPais2 != null) && oPais1 != oPais2) {
                                Pais.addFronteira(oPais1, oPais2);
                        }
                }
        }

        private static List<ArrayList<String>> getDataFromFile(String filePath, String fieldSeparator) {
                List<ArrayList<String>> data = new ArrayList<>();
                try {
                        BufferedReader reader = new BufferedReader(new FileReader(filePath));
                        String line = reader.readLine();
                        while (line != null) {
                                String[] lineData = line.split(fieldSeparator);
                                ArrayList<String> lineElem = new ArrayList<>();
                                for (String item : lineData) {
                                        lineElem.add(item.trim());
                                }
                                data.add(lineElem);
                                line = reader.readLine();
                        }
                } catch (IOException e) {
                        e.printStackTrace();
                }
                return data;
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="ESINF Exercício 2">
        public Map ordenarContinentePopulacao(Continente oContinente, double dblPopulacao) {
                TreeMap<Double, Pais> mapHabitantes = new TreeMap<>();
                for (Pais oPais : lstPaises) {
                        if (oPais.getContinente().equals(oContinente) && oPais.getPopulacao() > dblPopulacao) {
                                mapHabitantes.put(oPais.getPopulacao(), oPais);
                        }
                }
                for (Map.Entry entry : mapHabitantes.entrySet()) {
                        System.out.println("(" + entry.getKey() + "," + entry.getValue() + ")");
                }
                return mapHabitantes;
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="ESINF Exercício 3">
        public Map<Integer, List<Pais>> getPaisesFronteirasDecrescente(Continente oContinente) {
                Map<Integer, List<Pais>> mapFronteiras = new TreeMap<>(Collections.reverseOrder());
                List<Pais> lstPaisesByContinente = new ArrayList<>();
                for (Pais oPais : lstPaises) {
                        if (oPais.getContinente().equals(oContinente)) {
                                lstPaisesByContinente.add(oPais);
                        }
                }
                for (Pais oPais : lstPaisesByContinente) {
                        Integer nFronteiras = oPais.getFronteiras().size();
                        List<Pais> listKey = mapFronteiras.get(nFronteiras);
                        if (listKey == null) {
                                listKey = new ArrayList<>();
                        }
                        listKey.add(oPais);
                        mapFronteiras.put(nFronteiras, listKey);
                }
                for (Map.Entry entry : mapFronteiras.entrySet()) {
                        System.out.println(entry.getKey() + " -> " + entry.getValue());
                }
                return mapFronteiras;
        }
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="ESINF Exercício 4">
        public int getCaminhoMaisCurto(Pais oPais1, Pais oPais2) {
                Continente oContinente = oPais1.getContinente();
                if (!oContinente.equals(oPais2.getContinente())) {
                        System.out.println("Os Países encontram-se em continentes distintos.");
                        return -1;
                }
                if (oPais1.equals(oPais2)) {
                        System.out.println("Os Países são iguais.");
                        return 0;
                }
                List<Pais> lstPaisesByContinente = new ArrayList<>();
                for (Pais oPais : lstPaises) {
                        if (oPais.getContinente().equals(oContinente)) {
                                lstPaisesByContinente.add(oPais);
                        }
                }
                Integer nPaises = lstPaisesByContinente.size();
                Integer indexOrigem = lstPaisesByContinente.indexOf(oPais1);
                Integer indexDestino = lstPaisesByContinente.indexOf(oPais2);
                List<List<Integer>> matriz = new ArrayList<>();
                for (int i = 0; i < lstPaisesByContinente.size(); i++) {
                        ArrayList<Integer> tempList = new ArrayList<Integer>();
                        for (int j = 0; j < lstPaisesByContinente.size(); j++) {
                                if (lstPaisesByContinente.get(i).getFronteiras().contains(lstPaisesByContinente.get(j))) {
                                        tempList.add(1);
                                } else {
                                        tempList.add(0);
                                }
                        }
                        matriz.add(tempList);
                }
                if (!matriz.get(indexOrigem).contains(1) || !matriz.get(indexDestino).contains(1)) {
                        System.out.println("Pelo menos um dos países inseridos não tem fronteiras.");
                        return -1;
                }
                // Print Matriz Fronteiras
//                for (List<Integer> list : matriz) {
//                        System.out.println(list);
//                }
                int nFronteiras[] = new int[nPaises];
                Boolean isCalculado[] = new Boolean[nPaises];
                
                for (int i = 0; i < nPaises; i++) {
                        nFronteiras[i] = Integer.MAX_VALUE;
                        isCalculado[i] = false;
                }
                
                nFronteiras[indexOrigem] = 0;
                
                for (int n = 0; n < nPaises - 1; n++) {
                        int u = minDistance(nFronteiras, isCalculado);
                        isCalculado[u] = true;
                        for (int v = 0; v < nPaises; v++) {
                                if (!isCalculado[v] && matriz.get(u).get(v) != 0
                                        && nFronteiras[u] != Integer.MAX_VALUE && nFronteiras[u] + matriz.get(u).get(v) < nFronteiras[v]) {
                                        
                                        nFronteiras[v] = nFronteiras[u] + matriz.get(u).get(v);
                                }
                        }
                }
                
                if (nFronteiras[indexDestino] == Integer.MAX_VALUE) {
                        System.out.println("Não existe ligação terrestre entre os 2 países.");
                        return -1;
                }
                System.out.println(oPais1 + " -> " + oPais2 + " : " + nFronteiras[indexDestino]);
                return nFronteiras[indexDestino];
        }

        private int minDistance(int nFronteiras[], Boolean isCalculado[]) {
                // Inicializar Valores Mínimos
                int min = Integer.MAX_VALUE, indexMin = -1;
                
                for (int i = 0; i < nFronteiras.length; i++) {
                        if (isCalculado[i] == false && nFronteiras[i] <= min) {
                                min = nFronteiras[i];
                                indexMin = i;
                        }
                }
                return indexMin;
        }
    // </editor-fold>
}
