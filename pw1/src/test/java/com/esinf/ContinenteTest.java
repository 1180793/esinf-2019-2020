package com.esinf;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ContinenteTest {

        /**
         * Test of values method, of class Continente.
         */
        @Test
        public void testValues() {
                System.out.println("values");
                Continente[] expResult = {Continente.AMERICASUL, Continente.EUROPA};
                Continente[] result = Continente.values();
                assertArrayEquals(expResult, result);
        }

        /**
         * Test of valueOf method, of class Continente.
         */
        @Test
        public void testValueOf() {
                System.out.println("valueOf");
                String name = "EUROPA";
                Continente expResult = Continente.EUROPA;
                Continente result = Continente.valueOf(name);
                assertEquals(expResult, result);
        }

        /**
         * Test of getNome method, of class Continente.
         */
        @Test
        public void testGetNome() {
                System.out.println("getNome");
                Continente instance = Continente.AMERICASUL;
                String expResult = "AmericaSul";
                String result = instance.getNome();
                assertEquals(expResult, result);
        }

        /**
         * Test of getContinente method, of class Continente.
         */
        @Test
        public void testGetContinente() {
                System.out.println("getContinente");
                String strContinente = "americasul";
                Continente expResult = Continente.AMERICASUL;
                Continente result = Continente.getContinente(strContinente);
                assertEquals(expResult, result);
        }

        /**
         * Test of toString method, of class Continente.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                Continente instance = Continente.EUROPA;
                String expResult = "Europa";
                String result = instance.toString();
                assertEquals(expResult, result);
        }
        
}
