package com.esinf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;
import static org.junit.Assert.*;

public class PaisManagerTest {

        private List<Pais> lstPaises;
        private PaisManager manager;
        Pais oArgentina = new Pais("Argentina", Continente.AMERICASUL, 41.67, "Buenosaires", -34.61315, -58.37723);
        Pais oBolivia = new Pais("Bolivia", Continente.AMERICASUL, 9.7, "Lapaz", -16.5, -68.15);
        Pais oBrasil = new Pais("Brasil", Continente.AMERICASUL, 206.12, "Brasilia", -15.77972, -47.92972);
        Pais oChile = new Pais("Chile", Continente.AMERICASUL, 16.8, "Santiago", -33.45694, -70.64827);
        Pais oColombia = new Pais("Colombia", Continente.AMERICASUL, 46.86, "Bogota", 4.60971, -74.08175);
        Pais oEquador = new Pais("Equador", Continente.AMERICASUL, 14.88, "Quito", -0.22985, -78.52495);
        Pais oGuiana = new Pais("Guiana", Continente.AMERICASUL, 0.07, "Georgetwon", 6.80448, -58.15527);
        Pais oGuianaFrancesa = new Pais("Guianafrancesa", Continente.AMERICASUL, 2.88, "Caiena", 4.93333, -52.33333);
        Pais oParaguai = new Pais("Paraguai", Continente.AMERICASUL, 6.24, "Assuncao", -25.30066, -57.63591);
        Pais oPeru = new Pais("Peru", Continente.AMERICASUL, 28.22, "Lima", -12.04318, -77.02824);
        Pais oSuriname = new Pais("Suriname", Continente.AMERICASUL, 0.04, "Paramaribo", 5.86638, -55.16682);
        Pais oVenezuela = new Pais("Venezuela", Continente.AMERICASUL, 31.02, "Caracas", 10.48801, -66.87919);
        Pais oUruguai = new Pais("Uruguai", Continente.AMERICASUL, 3.35, "Montevideu", -34.90328, -56.18816);

        Pais oAlbania = new Pais("Albania", Continente.EUROPA, 2.88, "Tirana", 41.33165, 19.8318);
        Pais oAlemanha = new Pais("Alemanha", Continente.EUROPA, 82.8, "Berlim", 52.5234051, 13.4113999);
        Pais oArmenia = new Pais("Armenia", Continente.EUROPA, 3.01, "Erevan", 40.18111, 44.51361);
        Pais oAustria = new Pais("Austria", Continente.EUROPA, 8.77, "Viena", 48.2092062, 16.3727778);
        Pais oBelgica = new Pais("Belgica", Continente.EUROPA, 11.37, "Bruxelas", 50.8462807, 4.3547273);
        Pais oBielorussia = new Pais("Bielorussia", Continente.EUROPA, 9.48, "Minsk", 53.905117, 27.5611845);
        Pais oBosnia = new Pais("Bosnia", Continente.EUROPA, 3.75, "Sarajevo", 43.85643, 18.41342);
        Pais oBulgaria = new Pais("Bulgaria", Continente.EUROPA, 7.1, "Sofia", 42.6976246, 23.3222924);
        Pais oChipre = new Pais("Chipre", Continente.EUROPA, 0.85, "Nicosia", 35.167604, 33.373621);
        Pais oCroacia = new Pais("Croacia", Continente.EUROPA, 4.15, "Zagreb", 45.8150053, 15.9785014);
        Pais oDinamarca = new Pais("Dinamarca", Continente.EUROPA, 5.75, "Copenhaga", 55.6762944, 12.5681157);
        Pais oEslovaquia = new Pais("Eslovaquia", Continente.EUROPA, 5.44, "Bratislava", 48.1483765, 17.1073105);
        Pais oEslovenia = new Pais("Eslovenia", Continente.EUROPA, 2.06, "Liubliana", 46.0514263, 14.5059655);
        Pais oEspanha = new Pais("Espanha", Continente.EUROPA, 46.53, "Madrid", 40.4166909, -3.7003454);
        Pais oEstonia = new Pais("Estonia", Continente.EUROPA, 1.32, "Tallinn", 59.4388619, 24.7544715);
        Pais oFinlandia = new Pais("Finlandia", Continente.EUROPA, 5.5, "Helsinque", 60.1698791, 24.9384078);
        Pais oFranca = new Pais("Franca", Continente.EUROPA, 66.99, "Paris", 48.8566667, 2.3509871);
        Pais oGeorgia = new Pais("Georgia", Continente.EUROPA, 3.71, "Tbilisi", 41.709981, 44.792998);
        Pais oGrecia = new Pais("Grecia", Continente.EUROPA, 10.76, "Atenas", 37.97918, 23.716647);
        Pais oHolanda = new Pais("Holanda", Continente.EUROPA, 17.08, "Amsterdam", 52.3738007, 4.8909347);
        Pais oHungria = new Pais("Hungria", Continente.EUROPA, 9.8, "Budapeste", 47.4984056, 19.0407578);
        Pais oIrlanda = new Pais("Irlanda", Continente.EUROPA, 4.77, "Dublin", 53.344104, -6.2674937);
        Pais oIslandia = new Pais("Islandia", Continente.EUROPA, 0.34, "Reiquiavique", 64.135338, -21.89521);
        Pais oItalia = new Pais("Italia", Continente.EUROPA, 60.59, "Roma", 41.8954656, 12.4823243);
        Pais oKosovo = new Pais("Kosovo", Continente.EUROPA, 1.77, "Pristina", 42.672421, 21.164539);
        Pais oLetonia = new Pais("Letonia", Continente.EUROPA, 1.98, "Riga", 56.9465346, 24.1048525);
        Pais oLiechtenstein = new Pais("Liechtenstein", Continente.EUROPA, 0.04, "Vaduz", 47.1410409, 9.5214458);
        Pais oLituania = new Pais("Lituania", Continente.EUROPA, 2.85, "Vilnius", 54.6893865, 25.2800243);
        Pais oLuxemburgo = new Pais("Luxemburgo", Continente.EUROPA, 0.59, "Luxemburgo", 49.815273, 6.129583);
        Pais oMacedonia = new Pais("Macedonia", Continente.EUROPA, 2.07, "Escopia", 41.99646, 21.43141);
        Pais oMalta = new Pais("Malta", Continente.EUROPA, 0.44, "Valletta", 35.904171, 14.518907);
        Pais oMoldavia = new Pais("Moldavia", Continente.EUROPA, 3.55, "Chisinau", 47.026859, 28.841551);
        Pais oMonaco = new Pais("Monaco", Continente.EUROPA, 0.04, "Monaco", 43.750298, 7.412841);
        Pais oMontenegro = new Pais("Montenegro", Continente.EUROPA, 0.62, "Podgorica", 42.442575, 19.268646);
        Pais oNoruega = new Pais("Noruega", Continente.EUROPA, 5.26, "Oslo", 59.9138204, 10.7387413);
        Pais oPolonia = new Pais("Polonia", Continente.EUROPA, 38.42, "Varsovia", 52.2296756, 21.0122287);
        Pais oPortugal = new Pais("Portugal", Continente.EUROPA, 10.31, "Lisboa", 38.7071631, -9.135517);
        Pais oReinoUnido = new Pais("Reinounido", Continente.EUROPA, 65.81, "Londres", 51.5001524, -0.1262362);
        Pais oRepublicaCheca = new Pais("Republicacheca", Continente.EUROPA, 10.57, "Praga", 50.0878114, 14.4204598);
        Pais oRomenia = new Pais("Romenia", Continente.EUROPA, 19.64, "Bucareste", 44.430481, 26.12298);
        Pais oRussia = new Pais("Russia", Continente.EUROPA, 146.5, "Moscovo", 55.755786, 37.617633);
        Pais oServia = new Pais("Servia", Continente.EUROPA, 7.04, "Belgrado", 44.802416, 20.465601);
        Pais oSuecia = new Pais("Suecia", Continente.EUROPA, 10.0, "Estocolmo", 59.3327881, 18.0644881);
        Pais oSuica = new Pais("Suica", Continente.EUROPA, 8.42, "Berna", 46.9479986, 7.4481481);
        Pais oTurquia = new Pais("Turquia", Continente.EUROPA, 79.81, "Ancara", 39.91987, 32.85427);
        Pais oUcrania = new Pais("Ucrania", Continente.EUROPA, 42.59, "Kiev", 50.440951, 30.5271814);

        public PaisManagerTest() {
                this.manager = new PaisManager();
                manager.loadPaises("./src/test/resources/paises.txt");
                this.lstPaises = new ArrayList<>();
                lstPaises.addAll(Arrays.asList(oArgentina, oBolivia, oBrasil, oChile, oColombia, oEquador, oGuiana, oGuianaFrancesa,
                        oParaguai, oPeru, oSuriname, oVenezuela, oUruguai, oAlbania, oAlemanha, oArmenia, oAustria, oBelgica,
                        oBielorussia, oBosnia, oBulgaria, oChipre, oCroacia, oDinamarca, oEslovaquia, oEslovenia, oEspanha, oEstonia,
                        oFinlandia, oFranca, oGeorgia, oGrecia, oHolanda, oHungria, oIrlanda, oIslandia, oItalia, oKosovo, oLetonia,
                        oLiechtenstein, oLituania, oLuxemburgo, oMacedonia, oMalta, oMoldavia, oMonaco, oMontenegro, oNoruega, oPolonia,
                        oPortugal, oReinoUnido, oRepublicaCheca, oRomenia, oRussia, oServia, oSuecia, oSuica, oTurquia, oUcrania));
        }

        /**
         * Test of getPais method, of class PaisManager.
         */
        @Test
        public void testMatchPais() {
                System.out.println("matchPais");
                String strNome = "polonia";
                Pais expResult = new Pais("Polonia", Continente.EUROPA, 38.42, "Varsovia", 52.2296756, 21.0122287);
                Pais result = manager.matchPais(strNome);
                assertEquals(expResult, result);
        }

        /**
         * Test of loadPaises method, of class PaisManager.
         */
        @Test
        public void testLoadPaises() {
                System.out.println("loadPaises");
                assertEquals(manager.getPaises(), lstPaises);
        }

        /**
         * Test of loadFronteiras method, of class PaisManager.
         */
        @Test
        public void testLoadFronteiras() {
                System.out.println("loadFronteiras");
                manager.loadFronteiras("./src/test/resources/fronteiras.txt");
                oArgentina = manager.matchPais("argentina");
                oBrasil = manager.matchPais("brasil");
                oPortugal = manager.matchPais("portugal");
                assertEquals(true, oArgentina.getFronteiras().contains(oBrasil));
                assertEquals(false, oBrasil.getFronteiras().contains(oPortugal));
        }

        /**
         * Test of ordenarContinentePopulacao method, of class PaisManager.
         */
        @Test
        public void testOrdenarContinentePopulacao() {
                System.out.println("ordenarContinentePopulacao");
                Continente oContinente = Continente.EUROPA;
                double dblPopulacao = 50.0;
                Map expResult = new TreeMap();
                expResult.put(60.59, oItalia);
                expResult.put(65.81, oReinoUnido);
                expResult.put(66.99, oFranca);
                expResult.put(79.81, oTurquia);
                expResult.put(82.8, oAlemanha);
                expResult.put(146.5, oRussia);
                Map result = manager.ordenarContinentePopulacao(oContinente, dblPopulacao);
                assertEquals(expResult, result);
        }

        /**
         * Test of getPaisesFronteirasDecrescente method, of class PaisManager.
         */
        @Test
        public void testGetPaisesFronteirasDecrescente() {
                System.out.println("getPaisesFronteirasDecrescente");
                manager.loadFronteiras("./src/test/resources/fronteiras.txt");
                Continente oContinente = Continente.AMERICASUL;
                manager.getPaisesFronteirasDecrescente(oContinente);
                Map<Integer, List<Pais>> expResult = new TreeMap<>(Collections.reverseOrder());
                expResult.put(10, Arrays.asList(oBrasil));
                expResult.put(5, Arrays.asList(oArgentina, oBolivia, oPeru));
                expResult.put(4, Arrays.asList(oColombia));
                expResult.put(3, Arrays.asList(oChile, oGuiana, oParaguai, oSuriname, oVenezuela));
                expResult.put(2, Arrays.asList(oEquador, oGuianaFrancesa, oUruguai));
                Map<Integer, List<Pais>> result = manager.getPaisesFronteirasDecrescente(oContinente);
                assertEquals(expResult, result);
        }

        /**
         * Test of getCaminhoMaisCurto method, of class PaisManager.
         */
        @Test
        public void testGetCaminhoMaisCurto() {
                System.out.println("getCaminhoMaisCurto");
                manager.loadFronteiras("./src/test/resources/fronteiras.txt");
                int expResult = -1;
                int result = manager.getCaminhoMaisCurto(oPortugal, oBrasil);
                assertEquals(expResult, result);
                result = manager.getCaminhoMaisCurto(oPortugal, oMalta);
                assertEquals(expResult, result);
                result = manager.getCaminhoMaisCurto(oPortugal, oReinoUnido);
                assertEquals(expResult, result);
                expResult = 0;
                result = manager.getCaminhoMaisCurto(oPortugal, oPortugal);
                assertEquals(expResult, result);
                expResult = 2;
                result = manager.getCaminhoMaisCurto(oPortugal, oFranca);
                assertEquals(expResult, result);
                expResult = 6;
                result = manager.getCaminhoMaisCurto(oPortugal, oRussia);
                assertEquals(expResult, result);
        }

}
