package com.esinf;

import org.junit.Test;
import static org.junit.Assert.*;

public class PaisTest {

	private final Pais oPais;

	public PaisTest() {
		this.oPais = new Pais("Argentina", Continente.AMERICASUL, 41.67, "Buenosaires", -34.6131500, -58.3772300);
	}

	/**
	 * Test of getNome method, of class Pais.
	 */
	@Test
	public void testGetNome() {
		System.out.println("getNome");
		String expResult = "Argentina";
		String result = oPais.getNome();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getContinente method, of class Pais.
	 */
	@Test
	public void testGetContinente() {
		System.out.println("getContinente");
		Continente expResult = Continente.AMERICASUL;
		Continente result = oPais.getContinente();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getPopulacao method, of class Pais.
	 */
	@Test
	public void testGetPopulacao() {
		System.out.println("getPopulacao");
		double expResult = 41.67;
		double result = oPais.getPopulacao();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getCapital method, of class Pais.
	 */
	@Test
	public void testGetCapital() {
		System.out.println("getCapital");
		String expResult = "Buenosaires";
		String result = oPais.getCapital();
		assertEquals(expResult, result);
	}

	/**
	 * Test of getLatitude method, of class Pais.
	 */
	@Test
	public void testGetLatitude() {
		System.out.println("getLatitude");
		double expResult = -34.6131500;
		double result = oPais.getLatitude();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of getLongitude method, of class Pais.
	 */
	@Test
	public void testGetLongitude() {
		System.out.println("getLongitude");
		double expResult = -58.3772300;
		double result = oPais.getLongitude();
		assertEquals(expResult, result, 0.0);
	}

	/**
	 * Test of equals method, of class Pais.
	 */
	@Test
	public void testEquals() {
		System.out.println("equals");
		Pais pais2 = new Pais("Argentina", Continente.AMERICASUL, 41.67, "Buenosaires", -34.6131500, -58.3772300);
		boolean expResult = true;
		boolean result = oPais.equals(pais2);
		assertEquals(expResult, result);
		result = oPais.equals(oPais);
		assertEquals(expResult, result);
		expResult = false;
		result = oPais.equals(null);
		assertEquals(expResult, result);
		result = oPais.equals(new StringBuilder());
		assertEquals(expResult, result);
	}

	/**
	 * Test of toString method, of class Pais.
	 */
	@Test
	public void testToString() {
		System.out.println("toString");
//                String expResult = String.format("argentina (AmericaSul)%nPopulação: 41,67M%nCapital: buenosaires%nLatitude: -34,613150 | Longitude: -58,377230");
		String expResult = "Argentina";
		String result = oPais.toString();
		assertEquals(expResult, result);
	}

}
