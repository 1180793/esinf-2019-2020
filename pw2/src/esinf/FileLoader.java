package esinf;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class FileLoader {

        public static void loadPaises(PaisRegistry lstPaises, String filePath) {
                List<ArrayList<String>> list = getDataFromFile(filePath, ",");
                for (ArrayList<String> d : list) {
                        String strNome = d.get(0).substring(0, 1).toUpperCase() + d.get(0).substring(1);
                        String strContinente = d.get(1).substring(0, 1).toUpperCase() + d.get(1).substring(1);
                        double dblPopulacao = Double.parseDouble(d.get(2));
                        String strCapital = d.get(3).substring(0, 1).toUpperCase() + d.get(3).substring(1);
                        double dblLatitude = Double.parseDouble(d.get(4));
                        double dblLongitude = Double.parseDouble(d.get(5));
                        Pais oPais = new Pais(strNome, strContinente, dblPopulacao, strCapital, dblLatitude, dblLongitude);
                        lstPaises.addPais(oPais);
                }
        }

        public static void loadFronteiras(PaisRegistry lstPaises, String filePath) {
                List<ArrayList<String>> list = getDataFromFile(filePath, ",");
                for (ArrayList<String> d : list) {
                        String strPais1 = d.get(0);
                        String strPais2 = d.get(1);
                        Pais oPais1 = lstPaises.matchPais(strPais1);
                        Pais oPais2 = lstPaises.matchPais(strPais2);
                        if ((oPais1 != null && oPais2 != null) && oPais1 != oPais2) {
                                Pais.addFronteira(oPais1, oPais2);
                        }
                }
        }

        private static List<ArrayList<String>> getDataFromFile(String filePath, String fieldSeparator) {
                List<ArrayList<String>> data = new ArrayList<>();
                try {
                        BufferedReader reader = new BufferedReader(new FileReader(filePath));
                        String line = reader.readLine();
                        while (line != null) {
                                String[] lineData = line.split(fieldSeparator);
                                ArrayList<String> lineElem = new ArrayList<>();
                                for (String item : lineData) {
                                        lineElem.add(item.trim());
                                }
                                data.add(lineElem);
                                line = reader.readLine();
                        }
                } catch (IOException e) {
                        e.printStackTrace();
                }
                return data;
        }
}
