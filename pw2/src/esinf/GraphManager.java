package esinf;

import adjacencyMatrixGraph.AdjacencyMatrixGraph;
import adjacencyMatrixGraph.EdgeAsDoubleGraphAlgorithms;
import graph.Edge;
import graph.Graph;
import graph.GraphAlgorithms;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

public class GraphManager {

        public PaisRegistry oPaises;
        public Graph mGraph;
        public AdjacencyMatrixGraph mAdjencyMatrix;

        public GraphManager() {
                oPaises = new PaisRegistry();
                mGraph = new Graph(false);
                mAdjencyMatrix = new AdjacencyMatrixGraph();
                FileLoader.loadPaises(oPaises, "./paises.txt");
                FileLoader.loadFronteiras(oPaises, "./fronteiras.txt");
        }

        public PaisRegistry getPaisRegistry() {
                return oPaises;
        }

        /* 1. Construir o grafo de países e respetivas fronteiras a partir da informação fornecida nos ficheiros de
             texto. A capital de um país tem ligação direta com as capitais dos países com os quais faz fronteira. O
             cálculo da distância em Kms entre duas capitais deverá ser feito através das suas coordenadas.
         */
        public void buildGraph() {
                for (Pais oPais : oPaises.getPaises()) {
                        mGraph.insertVertex(oPais);
                        for (int i = 0; i < oPais.getFronteiras().size(); i++) {
                                Pais oPais2 = (Pais) oPais.getFronteiras().toArray()[i];
                                mGraph.insertEdge(oPais, oPais2, oPais.getCapital() + "-" + oPais2.getCapital(), Utils.calcDistanciaCapital(oPais, oPais2));
                        }

                }
        }

        public void buildAdjacencyMatrix() {
                for (Pais oPais : oPaises.getPaises()) {
                        mAdjencyMatrix.insertVertex(oPais);
                        for (int i = 0; i < oPais.getFronteiras().size(); i++) {
                                Pais oPais2 = (Pais) oPais.getFronteiras().toArray()[i];
                                mAdjencyMatrix.insertEdge(oPais, oPais2, Utils.calcDistanciaCapital(oPais, oPais2));
                        }
                }
        }

        /* 2. Colorir o mapa de tal modo que países vizinhos não partilhem a mesma cor e usando o menor número
             possível de cores.
         */
        public Map<Pais, String> colorGraph() {
                List<Pais> lstPaises = oPaises.getPaises();
                int[] iDegrees = new int[lstPaises.size()];
                for (int i = 0; i < iDegrees.length; i++) {
                        iDegrees[i] = mGraph.outDegree(lstPaises.get(i));
                }
                Utils.reverseQuickSort(iDegrees, (ArrayList) lstPaises, 0, iDegrees.length - 1);
                Map<Pais, String> rMapColors = new LinkedHashMap<>();
                int intColor = Integer.MAX_VALUE;
                while (intColor > 5) {
                        intColor = 1;
                        boolean bHasColor[] = new boolean[mGraph.numVertices()];
                        for (int i = 0; i < iDegrees.length; i++) {
                                if (!bHasColor[i]) {
                                        rMapColors.put(lstPaises.get(i), "Cor " + intColor);
                                        Set<Pais> set = new HashSet<>();
                                        set.add(lstPaises.get(i));
                                        bHasColor[i] = true;
                                        for (int j = i + 1; j < iDegrees.length; j++) {
                                                if (iDegrees[j] == 0 && !bHasColor[j]) {
                                                        rMapColors.put(lstPaises.get(j), "Cor " + intColor);
                                                        bHasColor[j] = true;
                                                }
                                                if (!bHasColor[j]) {
                                                        boolean isVizinho = false;
                                                        Iterator<Pais> it = set.iterator();
                                                        while (it.hasNext() && !isVizinho) {
                                                                Pais oPais2 = it.next();
                                                                if (mGraph.getEdge(lstPaises.get(j), oPais2) != null) {
                                                                        isVizinho = true;
                                                                }
                                                        }if (!isVizinho) {
                                                                rMapColors.put(lstPaises.get(j), "Cor " + intColor);
                                                                set.add(lstPaises.get(j));
                                                                bHasColor[j] = true;
                                                        }
                                                }
                                        }intColor++;
                                }
                        }
                        if (intColor > 5) {
                                Utils.reverseQuickSort(iDegrees, (ArrayList) lstPaises, 0, iDegrees.length - 1);
                                rMapColors.clear();
                        }
                }
                return rMapColors;
        }

        /* 3. Calcular o caminho mais curto entre duas capitais, deve indicar as capitais incluídas no caminho e a
             respetiva distância em kms.
         */
        public LinkedList<Pais> calculateShortestPath(Pais oPais1, Pais oPais2) {
                LinkedList<Pais> lstPaisesPercorridos = new LinkedList<>();
                double total = GraphAlgorithms.shortestPath(mGraph, oPais1, oPais2, lstPaisesPercorridos);
                System.out.println(total);
                if (lstPaisesPercorridos != null) {
                        System.out.printf("O Caminho mais curto entre '%s' e '%s' é: %.2f Km\n", oPais1.getNome(), oPais2.getNome(), total);
                        for (Pais oPais : lstPaisesPercorridos) {
                                System.out.print(oPais.getNome() + " -> ");
                        }
                }
                return lstPaisesPercorridos;
        }

        /* 4. Calcular o caminho mais curto entre duas capitais passando obrigatoriamente por outras capitais
             indicadas.
         */
        public LinkedList<Pais> calculateShortestPathCapitalObrigatoria(Pais vOrig, Pais vDest, List<Pais> lstPaisesObrigatorios) {
                ListIterator iterador = lstPaisesObrigatorios.listIterator();
                LinkedList<Pais> lstPath = new LinkedList<>();
                LinkedList<Pais> lastAuxTempPath = new LinkedList<>();
                lstPath.add(vOrig);

                while (iterador.hasNext()) {
                        Pais est = (Pais) iterador.next();
                        GraphAlgorithms.shortestPath(mGraph, lstPath.getLast(), est, lastAuxTempPath);

                        for (int i = 1; i < lastAuxTempPath.size(); i++) {
                                lstPath.add(lastAuxTempPath.get(i));
                        }
                        lastAuxTempPath.clear();
                }
                GraphAlgorithms.shortestPath(mGraph, lstPath.getLast(), vDest, lastAuxTempPath);

                for (int i = 1; i < lastAuxTempPath.size(); i++) {
                        lstPath.add(lastAuxTempPath.get(i));
                }

                System.out.println("");
                for (int i = 0; i < lstPath.size(); i++) {
                        System.out.print(lstPath.get(i) + " -> ");
                }
                System.out.println("");
                return lstPath;
        }

        /* 5. Determinar o circuito de menor comprimento que parte de uma capital origem e visita outras capitais
             uma única vez, voltando à capital inicial. Utilize a heurística do vizinho mais próximo: a próxima cidade
             a ser visitada é a mais próxima ainda não visitada. Identifique qual o número máximo de cidades
             possíveis de visitar usando esta heurística.
         */
        
        public int cycleClosestNeighbourHeuristic(Pais oPais, LinkedList<Pais> lstPath) {
                int path[];
                LinkedList<Pais> lstVizinhos = getFronteirasByPais(oPais);
                if (mGraph.outDegree(oPais) == 0) {
                        lstPath.add(oPais);
                        return 1;
                }
                if (mGraph.outDegree(oPais) == 1) {
                        lstPath.add(oPais);
                        Pais pFront = lstVizinhos.get(0);
                        lstPath.add(pFront);
                        lstPath.add(oPais);
                        return 3;
                }
                path = new int[mGraph.numVertices()];
                for (int i = 0; i < mGraph.numVertices(); i++) {
                        path[i] = -1;
                }
                path[0] = 0;
                if (hamCycleUtil(path, 1, lstPath) == false) {
                        System.out.println("\nSolution does not exist");
                        return 0;
                }
                return lstPath.size();
        }
        
        public LinkedList<Pais> getFronteirasByPais(Pais oPais) {
                LinkedList<Pais> lstFronteiras = new LinkedList<>();
                for (int i = 0; i < oPaises.getPaises().size(); i++) {
                        if (mGraph.getEdge(oPais, oPaises.getPaises().get(i)) != null) {
                                lstFronteiras.add(oPaises.getPaises().get(i));
                        }
                }
                lstFronteiras.sort(new Comparator<Pais>() {
                        @Override
                        public int compare(Pais oPais1, Pais oPais2) {
                                if (Utils.calcDistanciaCapital(oPais, oPais1) > Utils.calcDistanciaCapital(oPais, oPais2)) {
                                        return 1;
                                }
                                if (Utils.calcDistanciaCapital(oPais, oPais1) == Utils.calcDistanciaCapital(oPais, oPais2)) {
                                        return 0;
                                }
                                return -1;
                        }
                });
                return lstFronteiras;
        }

        boolean isSafe(Pais oPais, LinkedList<Pais> lstPath, Pais prevPais) {
                if (!getFronteirasByPais(oPais).contains(prevPais)) {
                        return false;
                }
                for (int i = 0; i < lstPath.size(); i++) {
                        if (lstPath.get(i) == oPais) {
                                return false;
                        }
                }

                return true;
        }

        boolean hamCycleUtil(int path[], int pos, LinkedList<Pais> lstPath) {
                if (pos == mGraph.numVertices()) {
                        if (getFronteirasByPais(getPaisByIndex(0)).contains(getPaisByIndex(pos - 1))) {
                                return true;
                        } else {
                                return false;
                        }
                }
                for (int v = 1; v < mGraph.numVertices(); v++) {
                        if (isSafe(getPaisByIndex(v), lstPath, getPaisByIndex(pos))) {
                                path[pos] = v;
                                
                                if (hamCycleUtil(path, pos + 1, lstPath) == true) {
                                        return true;
                                }
                                path[pos] = -1;
                        }
                }
                return false;
        }

        private Pais getPaisByIndex(int iIndex) {
                return oPaises.getPaises().get(iIndex);
        }
}
