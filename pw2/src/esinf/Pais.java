package esinf;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Pais {

        private String strNome;
        private String strContinente;
        private double dblPopulacao;
        private String strCapital;
        private double dblLatitude;
        private double dblLongitude;
        private Set setFronteiras;

        public Pais(String strNome, String strContinente, double dblPopulacao, String strCapital, double dblLatitude, double dblLongitude) {
                this.strNome = strNome;
                this.strContinente = strContinente;
                this.dblPopulacao = dblPopulacao;
                this.strCapital = strCapital;
                this.dblLatitude = dblLatitude;
                this.dblLongitude = dblLongitude;
                this.setFronteiras = new HashSet();
        }

        public String getNome() {
                return this.strNome;
        }

        public String getContinente() {
                return this.strContinente;
        }

        public double getPopulacao() {
                return this.dblPopulacao;
        }

        public String getCapital() {
                return this.strCapital;
        }

        public double getLatitude() {
                return this.dblLatitude;
        }

        public double getLongitude() {
                return this.dblLongitude;
        }

        public Set getFronteiras() {
                return this.setFronteiras;
        }

        private boolean addFronteira(Pais oPais) {
                return this.setFronteiras.add(oPais);
        }

        @Override
        public boolean equals(Object object) {
                if (this == object) {
                        return true;
                }
                if (object == null) {
                        return false;
                }
                if (getClass() != object.getClass()) {
                        return false;
                }
                Pais obj = (Pais) object;
                return (Objects.equals(strNome, obj.strNome));
        }

        @Override
        public String toString() {
//                StringBuilder builder = new StringBuilder();
//                String str = String.format("%s (%s)%nPopulação: %.2fM%nCapital: %s%nLatitude: %f | Longitude: %f", this.strNome, this.oContinente, this.dblPopulacao,
//                        this.strCapital, this.dblLatitude, this.dblLongitude);
//                builder.append(str);
//                return builder.toString();
                return this.strNome;
        }

        public static boolean addFronteira(Pais oPais1, Pais oPais2) {
                return (oPais1.addFronteira(oPais2)
                        && oPais2.addFronteira(oPais1));
        }
}
