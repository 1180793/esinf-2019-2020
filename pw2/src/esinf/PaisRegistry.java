package esinf;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Goncalo
 */
public class PaisRegistry {
    
    private List<Pais> lstPaises;
    
    public PaisRegistry(){
        this.lstPaises = new ArrayList<>();
    }
    
    public void addPais(Pais oPais){
        lstPaises.add(oPais);
    }
    public Pais matchPais(String strNome){
        for (Pais oPais : lstPaises) {
            if (oPais.getNome().equalsIgnoreCase(strNome)) {
                                return oPais;
                        }
        }
        return null;
    }

    /**
     * @return the lstPaises
     */
    public List<Pais> getPaises() {
        return lstPaises;
    }

    /**
     * @param lstPaises2 the lstPaises to set
     */
    public void setListaEstacoes(List<Pais> lstPaises2) {
        this.lstPaises = lstPaises2;
    }
    
    

}
