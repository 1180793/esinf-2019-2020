package esinf;

import java.util.Comparator;
import graph.Graph;
import java.util.ArrayList;
import java.util.LinkedList;

public class Utils {

        public static double calcDistanciaCapital(Pais oPais1, Pais oPais2) {
                final int r = 6371;

                double dblLatitude = Math.toRadians(oPais2.getLatitude() - oPais1.getLatitude());
                double dblLongitude = Math.toRadians(oPais2.getLongitude() - oPais1.getLongitude());

                double a = Math.sin(dblLatitude / 2) * Math.sin(dblLatitude / 2)
                        + Math.cos(Math.toRadians(oPais1.getLatitude())) * Math.cos(Math.toRadians(oPais2.getLatitude()))
                        * Math.sin(dblLongitude / 2) * Math.sin(dblLongitude / 2);

                double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

                double dblDistance = r * c;
                dblDistance = Math.abs(dblDistance);

                return dblDistance;
        }

        public static void reverseQuickSort(int[] array, ArrayList<Pais> arrayList, int left, int right) {
                int pivot = array[(left + right) / 2];
                int i = left;
                int j = right;
                int aux;
                Pais auxPais;
                while (i <= j) {
                        while (array[i] > pivot) {
                                i++;
                        }
                        while (array[j] < pivot) {
                                j--;
                        }
                        if (i <= j) {
                                aux = array[i];
                                array[i] = array[j];
                                array[j] = aux;

                                auxPais = arrayList.get(i);
                                arrayList.set(i, arrayList.get(j));
                                arrayList.set(j, auxPais);

                                i++;
                                j--;
                        }
                }
                if (left < j) {
                        reverseQuickSort(array, arrayList, left, j);
                }
                if (right > i) {
                        reverseQuickSort(array, arrayList, i, right);
                }
        }
}
