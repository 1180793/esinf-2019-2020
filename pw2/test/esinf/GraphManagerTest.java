package esinf;

import graph.Graph;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Goncalo
 */
public class GraphManagerTest {

        GraphManager oManager;
        
        @Before
        public void setUp() {
                oManager = new GraphManager();
        }

        /**
         * Test of buildGraph method, of class GraphManager.
         */
        @Test
        public void testBuildGraph() {
                System.out.println("Test Build Graph");
                assertTrue("The Graph should be empty before build", oManager.mGraph.numVertices() == 0);
                oManager.buildGraph();
                assertTrue("The Graph shouldn't be empty after build", oManager.mGraph.numVertices() != 0);
        }

        /**
         * Test of colorGraph method, of class GraphManager.
         */
        @Test
        public void testColorGraph() {
                oManager.buildGraph();
                System.out.println("Test Color Graph");
                Pais oBrasil = oManager.getPaisRegistry().matchPais("Brasil");
                Pais oArgentina = oManager.getPaisRegistry().matchPais("Argentina");
                Pais oPortugal = oManager.getPaisRegistry().matchPais("Portugal");
                Pais oEspanha = oManager.getPaisRegistry().matchPais("Espanha");
                Pais oFranca = oManager.getPaisRegistry().matchPais("Franca");
                Map<Pais, String> oMapColors = oManager.colorGraph();
                assertTrue("Brasil and Argentina shoudn't have the same color", !oMapColors.get(oBrasil).equalsIgnoreCase(oMapColors.get(oArgentina)));
                assertTrue("Portugal and Espanha shoudn't have the same color", !oMapColors.get(oPortugal).equalsIgnoreCase(oMapColors.get(oEspanha)));
                assertTrue("Espanha and Franca shoudn't have the same color", !oMapColors.get(oEspanha).equalsIgnoreCase(oMapColors.get(oFranca)));
        }

        /**
         * Test of calculateShortestPath method, of class GraphManager.
         */
        @Test
        public void testCalculateShortestPath() {
                oManager.buildGraph();
                System.out.println("Test Calculate Shortest Path");
                Pais oPortugal = oManager.getPaisRegistry().matchPais("Portugal");
                Pais oEspanha = oManager.getPaisRegistry().matchPais("Espanha");
                Pais oFranca = oManager.getPaisRegistry().matchPais("Franca");
                Pais oAlemanha = oManager.getPaisRegistry().matchPais("Alemanha");
                Pais oPolonia = oManager.getPaisRegistry().matchPais("Polonia");
                //oPortugal-oEspanha-oFranca-oAlemanha-oPolonia         Shortest Path
                LinkedList<Pais> result = oManager.calculateShortestPath(oPortugal, oPolonia);
                System.out.println(result);
                Iterator<Pais> it = result.iterator();
                assertTrue("First in path should be Portugal", it.next().equals(oPortugal));
                assertTrue("then Espanha", it.next().equals(oEspanha));
                assertTrue("then Franca", it.next().equals(oFranca));
                assertTrue("then Alemanha", it.next().equals(oAlemanha));
                assertTrue("then Polonia", it.next().equals(oPolonia));
        }

        /**
         * Test of calculateShortestPathCapitalObrigatoria method, of class GraphManager.
         */
        @Test
        public void testCalculateShortestPathCapitalObrigatoria() {
                oManager.buildGraph();
                System.out.println("Test Calculate Shortest Path Capital Obrigatoria");
                Pais oPortugal = oManager.getPaisRegistry().matchPais("Portugal");
                Pais oEspanha = oManager.getPaisRegistry().matchPais("Espanha");
                Pais oFranca = oManager.getPaisRegistry().matchPais("Franca");
                Pais oAlemanha = oManager.getPaisRegistry().matchPais("Alemanha");
                Pais oPolonia = oManager.getPaisRegistry().matchPais("Polonia");
                Pais oSuica = oManager.getPaisRegistry().matchPais("Suica");
                ArrayList<Pais> lstMustPass = new ArrayList<>();
                lstMustPass.add(oSuica);
                //oPortugal-oEspanha-oFranca-oAlemanha-oPolonia                         Shortest Path
                //Portugal -> Espanha -> Franca -> Suica -> Alemanha -> Polonia ->      Shortest Path que passa pela Suiça
                LinkedList<Pais> result = oManager.calculateShortestPathCapitalObrigatoria(oPortugal, oPolonia, lstMustPass);
                System.out.println(result);
                Iterator<Pais> it = result.iterator();
                assertTrue("First in path should be Portugal", it.next().equals(oPortugal));
                assertTrue("then Espanha", it.next().equals(oEspanha));
                assertTrue("then Franca", it.next().equals(oFranca));
                assertTrue("then Suica", it.next().equals(oSuica));
                assertTrue("then Alemanha", it.next().equals(oAlemanha));
                assertTrue("then Polonia", it.next().equals(oPolonia));
        }

}
