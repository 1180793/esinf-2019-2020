package bst;

import esinf.Pais;
import esinf.Utils;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Pais2DTree extends BST<Pais> {

        protected static class Area {

                // Y - Latitude [-90,90]
                // X - Longitude [-180,180]
                private final double minLongitude, minLatitude;
                private final double maxLongitude, maxLatitude;

                public Area(double minLatitude, double minLongitude, double maxLatitude, double maxLongitude) {
                        if (minLatitude > maxLatitude) {
                                this.minLatitude = maxLatitude;
                                this.maxLatitude = minLatitude;
                        } else {
                                this.minLatitude = minLatitude;
                                this.maxLatitude = maxLatitude;
                        }
                        if (minLongitude > maxLongitude) {
                                this.minLongitude = maxLongitude;
                                this.maxLongitude = minLongitude;
                        } else {
                                this.minLongitude = minLongitude;
                                this.maxLongitude = maxLongitude;
                        }

                }

                public double getMinLongitude() {
                        return this.minLongitude;
                }

                public double getMaxLongitude() {
                        return this.maxLongitude;
                }

                public double getMinLatitude() {
                        return this.minLatitude;
                }

                public double getMaxLatitude() {
                        return this.maxLatitude;
                }

                public boolean intersects(Area oArea) {
                        return this.maxLatitude >= oArea.minLatitude && this.maxLongitude >= oArea.minLongitude
                                && oArea.maxLatitude >= this.minLatitude && oArea.maxLongitude >= this.minLongitude;
                }

                public boolean contains(Pais oPais) {
                        return (oPais.getLatitude() >= this.minLatitude) && (oPais.getLatitude() <= this.maxLatitude)
                                && (oPais.getLongitude() >= this.minLongitude) && (oPais.getLongitude() <= this.maxLongitude);
                }
        }

        // X - Latitude [-90,90]
        // Y - Longitude [-180,180]
        private static final Area oPlanisferio = new Area(-90, -180, 90, 180);

        @Override
        public void insert(Pais pais) {
                root = insert(pais, root);
        }

        private Node<Pais> insert(Pais pais, Node<Pais> node) {
                if (node == null) {
                        return new Node(pais, null, null);
                }
                int cmpResult = comparePoints(pais.getLatitude(), pais.getLongitude(), node);
                if (cmpResult < 0) {
                        node.setLeft(insert(pais, node.getLeft()));
                        return node;
                } else if (cmpResult > 0) {
                        node.setRight(insert(pais, node.getRight()));
                        return node;
                }
                node.setElement(pais);
                return node;
        }

        private int comparePoints(double latitude, double longitude, BST.Node<Pais> n) {
                if (isEven(height(n))) {
                        return compareLatitude.compare(n.getElement().getLatitude(), latitude);
                } else {
                        return compareLongitude.compare(n.getElement().getLongitude(), longitude);
                }
        }

        private final Comparator<Double> compareLatitude = new Comparator<Double>() {
                @Override
                public int compare(Double p1, Double p2) {
                        return Double.compare(p1, p2);
                }
        };

        private final Comparator<Double> compareLongitude = new Comparator<Double>() {
                @Override
                public int compare(Double p1, Double p2) {
                        return Double.compare(p1, p2);
                }
        };

        private boolean isEven(int height) {
                return height % 2 == 0;
        }

        public Pais getPaisByCoordenadas(double dblLatitude, double dblLongitude) {
                return find(root, dblLatitude, dblLongitude, true);
        }

        private Pais find(Node<Pais> node, double latitude, double longitude, boolean isEven) {
                if (node.getElement().getLongitude() == longitude && node.getElement().getLatitude() == latitude) {
                        return node.getElement();
                }
                if (isEven && latitude < node.getElement().getLatitude() || !isEven && longitude < node.getElement().getLongitude()) {
                        return find(node.getLeft(), latitude, longitude, !isEven);
                } else {
                        return find(node.getRight(), latitude, longitude, !isEven);
                }
        }

        public Pais closestNeighbour(double latitude, double longitude) {
                return closestNeighbour(root, latitude, longitude, root.getElement());
        }

        private Pais closestNeighbour(BST.Node<Pais> node, double latitude, double longitude, Pais currentClosest) {
                if (node == null) {
                        return currentClosest;
                }
                if (node.getElement().getLatitude() == (latitude) && node.getElement().getLongitude() == (longitude)) {
                        return node.getElement();
                }
                // Determina se o Pais atual está mais perto que o currentClosest
                if (Utils.calcDistanciaPais(node.getElement(), latitude, longitude) < Utils.calcDistanciaPais(currentClosest, latitude, longitude)) {
                        currentClosest = node.getElement();
                }
                // Determina para que filho do node atual vai ser efetuada a chamada recursiva
                double cmpValue = comparePoints(latitude, longitude, node);

                //Se for inferior vai para o filho esquerdo
                if (cmpValue < 0) {
                        currentClosest = closestNeighbour(node.getLeft(), latitude, longitude, currentClosest);
                        // Se a distancia do atual melhor for superior ou igual ao do termo de comparaçao vamos para o filho direito
                        if (Utils.calcDistanciaPais(currentClosest, latitude, longitude) >= cmpValue) {
                                currentClosest = closestNeighbour(node.getRight(), latitude, longitude, currentClosest);
                        }
                } //Se não for inferior vai para o filho direito
                else {
                        currentClosest = closestNeighbour(node.getRight(), latitude, longitude, currentClosest);
                        // currentClosest pode ter sido alterado por isso recalculamos e comparamos
                        if (Utils.calcDistanciaPais(currentClosest, latitude, longitude) >= cmpValue) {
                                currentClosest = closestNeighbour(node.getLeft(), latitude, longitude, currentClosest);
                        }
                }

                return currentClosest;
        }

        public List<Pais> areaSearch(double dblLatitude, double dblLongitude, double dblLatitude2, double dblLongitude2) {
                Area oArea = new Area(dblLatitude, dblLongitude, dblLatitude2, dblLongitude2);
                List<Pais> result = new ArrayList<>();
                areaSearch(root, oArea, result);
                return result;
        }

        private void areaSearch(Node<Pais> node, Area oArea, List<Pais> result) {
                if (node != null) {
                        if (oArea.contains(node.getElement())) {
                                result.add(node.getElement());
                        }
                }
                if (node.getLeft() != null) {
                        areaSearch(node.getLeft(), oArea, result);
                }
                if (node.getRight() != null) {
                        areaSearch(node.getRight(), oArea, result);
                }
        }

}
