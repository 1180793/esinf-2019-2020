package esinf;

import bst.BST;
import bst.Pais2DTree;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Exercicios {

        public BST bstTree;
        public Pais2DTree kdTree;
        public List<Pais> lstLoadedPaises;

        public Exercicios() {
                bstTree = new BST();
                kdTree = new Pais2DTree();
                lstLoadedPaises = FileLoader.getPaisesList("./paises.txt", "./fronteiras.txt");
        }

        /*
         * Retorna a Binary Search Tree.
         */
        public BST getBST() {
                return bstTree;
        }

        /*
         * Retorna a 2DTree.
         */
        public Pais2DTree get2DTree() {
                return kdTree;
        }

        /*
         * 1. a) Contruir uma árvore binária de pesquisa que contenha a informação relativa a cada país: nome, continente, 
         *       população, capital, latitude, longitude, lista de países fronteira e número total de fronteiras.
         */
        public void buildBST() {
                for (Pais oPais : lstLoadedPaises) {
                        bstTree.insert(oPais);
                }
        }

        /*
         * 1. a) Forneça um método que, dado um nome de país, devolva a lista das suas fronteiras com
         *       outros países.
         */
        public List<Pais> getFronteirasByPais(String strNomePais) {
                Pais oPais = matchPais(strNomePais);
                List<Pais> lstFronteiras = new ArrayList<>(oPais.getFronteiras());
                return lstFronteiras;
        }

        /*
         * Percorre a BST e retorna o Pais(objeto) cujo nome é igual ao que foi passado por parametro.
         */
        private Pais matchPais(String strNome) {
                Iterable<Pais> it = bstTree.inOrder();
                for (Pais oPais : it) {
                        if (oPais.getNome().equalsIgnoreCase(strNome)) {
                                return oPais;
                        }
                }
                return null;
        }

        /*
         * 1. b) Fornecer um método capaz de devolver uma lista ordenada dos países pertencentes a um
         *       determinado continente. Efectuar ordenação decrescente por número de fronteiras e crescente
         *       por valor de população (i.e.: maior número de fronteiras e menor população). 
         */
        public List<Pais> getPaisesByContinenteOrdenados(String strContinente) {
                List<Pais> lstPaisesByContinente = getPaisesByContinente(strContinente);
                if (lstPaisesByContinente.isEmpty()) {
                        return null;    // Continente não existe
                }
                Collections.sort(lstPaisesByContinente, new Comparator<Pais>() {
                        @Override
                        public int compare(Pais oPais1, Pais oPais2) {
                                int nFronteiras = Integer.compare(oPais1.getFronteiras().size(), oPais2.getFronteiras().size());
                                if (nFronteiras > 0) {
                                        return -1;
                                } else if (nFronteiras < 0) {
                                        return 1;
                                } else {
                                        double nPopulacao = Double.compare(oPais1.getPopulacao(), oPais2.getPopulacao());
                                        if (nPopulacao < 0) {
                                                return -1;
                                        } else if (nPopulacao > 0) {
                                                return 1;
                                        }
                                }
                                return 0;
                        }
                });
                return lstPaisesByContinente;
        }

        /*
         * Retorna uma lista dos paises cujo continente é igual ao que foi passado por parametro.
         */
        private List<Pais> getPaisesByContinente(String strContinente) {
                Iterable<Pais> it = bstTree.inOrder();
                List<Pais> lstPais = new ArrayList<>();
                for (Pais oPais : it) {
                        if (oPais.getContinente().equalsIgnoreCase(strContinente)) {
                                lstPais.add(oPais);
                        }
                }
                return lstPais;
        }

        /*
         * 2. a) Fornecer um método capaz de devolver uma lista ordenada dos países pertencentes a um
         *       determinado continente. Efectuar ordenação decrescente por número de fronteiras e crescente
         *       por valor de população (i.e.: maior número de fronteiras e menor população). 
         */
        public void build2DTree() {
                for (Pais oPais : lstLoadedPaises) {
                        kdTree.insert(oPais);
                }

        }

        /*
         * 2. b) Pesquisa exata: utilizando a 2d-tree da alínea a), fornecer um método que devolva o país cuja 
         *       capital está situada nas coordenadas (latitude, longitude). 
         */
        public Pais getPaisFromCoordenadas(double dblLatitude, double dblLongitude) {
                Pais oPais = kdTree.getPaisByCoordenadas(dblLatitude, dblLongitude);
                return oPais;
        }
        
        /*
         * 2. c) Pesquisa vizinho mais próximo: utilizando a 2d-tree da alínea a), fornecer um método que encontre
         *       o país cuja capital está mais próxima das coordenadas (latitude, longitude).
        */
        public Pais getClosestPais(double dblLatitude, double dblLongitude) {
                Pais oPais = kdTree.closestNeighbour(dblLatitude, dblLongitude);
                return oPais;
        }
        
        /*
         * 2. d) Pesquisa por área geográfica: Utilizando a 2d-tree da alínea a), fornecer um método que devolva a
         *       lista de todos os países cuja capital está contida numa área dada por um rectângulo de coordenadas
         *       (latitude1, longitude1) e (latitude2, longitude2).
        */
        public List<Pais> getPaisesInArea (double dblLatitude, double dblLongitude, double dblLatitude2, double dblLongitude2) {
                List<Pais> lstPaises = kdTree.areaSearch(dblLatitude, dblLongitude, dblLatitude2, dblLongitude2);
                return lstPaises;
        }
        
}
