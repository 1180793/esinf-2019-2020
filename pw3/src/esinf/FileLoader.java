package esinf;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileLoader {

        public static List<Pais> getPaisesList(String filePathPaises, String filePathFronteiras) {
                List<Pais> lstPaises = loadPaises(filePathPaises);
                loadFronteiras(lstPaises, filePathFronteiras);
                return lstPaises;
        }
        
        private static Pais matchPais(String strNome, List<Pais> lstPaises) {
                for (Pais oPais : lstPaises) {
                        if (oPais.getNome().equalsIgnoreCase(strNome)) {
                                return oPais;
                        }
                }
                return null;
        }

        private static List<Pais> loadPaises(String filePath) {
                List<Pais> lstPaises = new ArrayList<>();
                List<ArrayList<String>> list = getDataFromFile(filePath, ",");
                for (ArrayList<String> data : list) {
                        Pais oPais = readPais(data);
                        lstPaises.add(oPais);
                }
                return lstPaises;
        }

        private static Pais readPais(ArrayList<String> d) {
                String strNome = d.get(0).substring(0, 1).toUpperCase() + d.get(0).substring(1);
                String strContinente = d.get(1).substring(0, 1).toUpperCase() + d.get(1).substring(1);
                double dblPopulacao = Double.parseDouble(d.get(2));
                String strCapital = d.get(3).substring(0, 1).toUpperCase() + d.get(3).substring(1);
                double dblLatitude = Double.parseDouble(d.get(4));
                double dblLongitude = Double.parseDouble(d.get(5));
                Pais oPais = new Pais(strNome, strContinente, dblPopulacao, strCapital, dblLatitude, dblLongitude);

                return oPais;
        }

        private static void loadFronteiras(List lstPaises, String filePath) {
                List<ArrayList<String>> list = getDataFromFile(filePath, ",");
                for (ArrayList<String> d : list) {
                        String strPais1 = d.get(0);
                        String strPais2 = d.get(1);
                        Pais oPais1 = matchPais(strPais1, lstPaises);
                        Pais oPais2 = matchPais(strPais2, lstPaises);
                        if ((oPais1 != null && oPais2 != null) && oPais1 != oPais2) {
                                Pais.addFronteira(oPais1, oPais2);
                        }
                }
        }

        private static List<ArrayList<String>> getDataFromFile(String filePath, String fieldSeparator) {
                List<ArrayList<String>> data = new ArrayList<>();
                try {
                        BufferedReader reader = new BufferedReader(new FileReader(filePath));
                        String line = reader.readLine();
                        while (line != null) {
                                String[] lineData = line.split(fieldSeparator);
                                ArrayList<String> lineElem = new ArrayList<>();
                                for (String item : lineData) {
                                        lineElem.add(item.trim());
                                }
                                data.add(lineElem);
                                line = reader.readLine();
                        }
                } catch (IOException e) {
                        e.printStackTrace();
                }
                return data;
        }
}
