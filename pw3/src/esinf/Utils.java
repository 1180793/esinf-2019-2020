package esinf;

public class Utils {

        public static double calcDistanciaPais(Pais oPais1, double latitude, double longitude) {
                final int r = 6371;

                double dblLatitude = Math.toRadians(latitude - oPais1.getLatitude());
                double dblLongitude = Math.toRadians(longitude - oPais1.getLongitude());

                double a = Math.sin(dblLatitude / 2) * Math.sin(dblLatitude / 2)
                        + Math.cos(Math.toRadians(oPais1.getLatitude())) * Math.cos(Math.toRadians(latitude))
                        * Math.sin(dblLongitude / 2) * Math.sin(dblLongitude / 2);

                double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

                double dblDistance = r * c;
                dblDistance = Math.abs(dblDistance);

                return dblDistance;
        }
}
