package esinf;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class ExerciciosTest {

        Exercicios oExercicios;

        @Before
        public void setUp() {
                oExercicios = new Exercicios();
        }

        /**
         * Test of buildBST method, of class Exercicios.
         */
        @Test
        public void testBuildBST() {
                System.out.println("Teste BuildBST");
                oExercicios.buildBST();
                System.out.println(oExercicios.getBST());
                assertTrue("A BST não contem os 59 países existentes nos ficheiros", oExercicios.getBST().size() == 59);
        }

        /**
         * Test of getFronteirasByPais method, of class Exercicios.
         */
        @Test
        public void testGetFronteirasByPais() {
                System.out.println("Teste GetFronteirasByPais");
                oExercicios.buildBST();
                Iterable<Pais> itPaises = oExercicios.getBST().inOrder();
                for (Pais oPais : itPaises) {
                        String strPais = oPais.getNome();
                        Iterable<Pais> itFronteiras = oPais.getFronteiras();
                        List<Pais> lstFronteiras = oExercicios.getFronteirasByPais(strPais);
                        for (Pais oPaisFronteira : itFronteiras) {
                                assertTrue("'" + strPais + "' deveria retornar '" + oPaisFronteira.getNome() + "' como fronteira.", lstFronteiras.contains(oPaisFronteira));
                        }
                }
        }

        /**
         * Test of getPaisesByContinenteOrdenados method, of class Exercicios.
         */
        @Test
        public void testGetPaisesByContinenteOrdenados() {
                System.out.println("Teste GetPaisesByContinenteOrdenados");
                oExercicios.buildBST();
                String strContinente = "europa";
                List<Pais> result = oExercicios.getPaisesByContinenteOrdenados(strContinente);
                for (int i = 1; i < result.size(); i++) {
                        Pais oPaisAnterior = result.get(i - 1);
                        Pais oPais = result.get(i);
                        boolean isOrdered = isOrdered(oPaisAnterior, oPais);
                        assertTrue(isOrdered);
                }
        }

        private boolean isOrdered(Pais oPaisAnterior, Pais oPais) {
                if (oPaisAnterior.getFronteirasSize() < oPais.getFronteirasSize()) {
                        System.out.println("'" + oPaisAnterior.getNome() + "' should be after '" + oPais.getNome() + "'");
                        return false;
                } else if (oPaisAnterior.getFronteirasSize() > oPais.getFronteirasSize()) {
                        return true;
                } else {
                        if (oPaisAnterior.getPopulacao() > oPais.getPopulacao()) {
                                System.out.println("'" + oPaisAnterior.getNome() + "' should be after '" + oPais.getNome() + "'");
                                return false;
                        }
                }
                return true;
        }

        @Test
        public void testBuild2DTree() {
                System.out.println("Teste Build2DTree");
                oExercicios.build2DTree();
                System.out.println(oExercicios.get2DTree());
                assertTrue("A 2DTree não contem os 59 países existentes nos ficheiros", oExercicios.get2DTree().size() == 59);
        }
        
        @Test
        public void testGetPaisFromCoordenadas() {
                System.out.println("Teste GetPaisFromCoordenadas");
                oExercicios.build2DTree();
                for (Pais oPais : oExercicios.lstLoadedPaises) {
                        assertTrue("As coordenadas deveriam corresponder ao '" + oPais.getNome() + "'", oExercicios.getPaisFromCoordenadas(oPais.getLatitude(), oPais.getLongitude()).equals(oPais));
                }
        }

        @Test
        public void testGetClosestPais() {
                System.out.println("Teste GetClosestCountry");
                oExercicios.build2DTree();
                for (Pais oPais : oExercicios.lstLoadedPaises) {
                        assertTrue("As coordenadas deveriam corresponder ao '" + oPais.getNome() + "'", oExercicios.getClosestPais(oPais.getLatitude() - 0.1, oPais.getLongitude() - 0.1).equals(oPais));
                }
        }
        
        @Test
        public void testGetPaisesInArea() {
                System.out.println("Teste GetPaisesInArea");
                oExercicios.build2DTree();
                List<Pais> lstPaisesInArea = oExercicios.getPaisesInArea(-34.6131500, -58.3772300, -0.2298500, -78.5249500);
                List<Pais> expectedList = new ArrayList<>();
                System.out.println(lstPaisesInArea);
                expectedList.add(matchPais("Argentina"));
                expectedList.add(matchPais("Bolivia"));
                expectedList.add(matchPais("Chile"));
                expectedList.add(matchPais("Peru"));
                expectedList.add(matchPais("Equador"));
                assertTrue("A lista não tem o tamanho correto", lstPaisesInArea.size() == expectedList.size());
                for (Pais oPais : lstPaisesInArea) {
                        assertTrue("A lista deveria conter '" + oPais.getNome() + "'.", expectedList.contains(oPais));
                }
                // Y - Latitude [-90,90]
                // X - Longitude [-180,180]
                List<Pais> lstPaisesInPlanisferio = oExercicios.getPaisesInArea(-90, -180, 90, 180);
                int size = oExercicios.get2DTree().size();
                assertTrue("A lista não contem os 59 países existentes nos ficheiros", lstPaisesInPlanisferio.size() == size);
        }
        
        private Pais matchPais(String strNome) {
                Iterable<Pais> it = oExercicios.get2DTree().inOrder();
                for (Pais oPais : it) {
                        if (oPais.getNome().equalsIgnoreCase(strNome)) {
                                return oPais;
                        }
                }
                return null;
        }
        
}
